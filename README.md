# micluster
*mergerfs-iscsi-cluster*, a simple clustered filesystem implementation using iscsi and mergerfs. Only tested on Archlinux.

## Description
micluster wakes up remote pcs using wake-on-lan. When the hosts are pingable an iscsi discovery and login is executed. Then the attached disks are mounted in temporary directories and those are merged using mergerfs into the destination directory.
When micluster stops it unmounts everything and can execute a command on the iscsi-target-server, e.g. a shutdown command.

## Requirements
- openssh
- open-iscsi
- python
- python-pip
- python-xattr
- mergerfs

pip packages:
- ping3
- pywol

## Installation
move the scripts to somewhere in PATH, e.g. to /usr/bin/

## Configuration
- example for main configuration file
  ```
  /etc/micluster/config<hr>{
      "data": {
          "targets": [
              {
                  "host": "<dns name or ip of target server>",
                  "mac": "<mac of target server>"
              }
          ],
          "destination": "/shares",
          "ping_refresh": 60,
          "discovery_refresh": 120,
          "mount_refresh": 120,
          "merge_refresh": 10,
          "mergerfs_options": ["security_capability=false"],
          "ssh_tries": 60,
          "ssh_config": "/etc/micluster/ssh_data.conf",
          "ssh_command": "sudo poweroff"
      },
      "backup" {
          "targets": [...],
          ...
      }
  }
  ```
  explanation
  - data, backup: cluster names
  - targets: array of target servers consiting of ip/dns and mac address
  - destination: the destination mount folder
  - ping_refresh: seconds after which a server is pinged again after a successful ping
  - discovery_refresh: seconds after which iscsi discovery is repeated after successful discovery
  - mount_refresh: seconds after which the temporary mounts of the iscsi disks are tested
  - merge_refresh: seconds after which the mergerfs mount is checked
  - mergerfs_options: additional options for mergerfs mount, implied options are 'allow_other', 'use_ino' and 'posix_acl=true'
  - ssh_tries: how often the ssh shutdown should be tried (it's tried every second)
  - ssh_config: ssh configuration file for the targets of the cluster, example below
  - ssh_command: the command to be executed by ssh (e.g. `sudo poweroff` or `sudo touch /shutdown_wanted`), assure that the user has permissions to execute the command
  - *the ssh_\* entries are optional, either don't specify any ssh entry or specify all*
- example for ssh configuration file
  ```
  /etc/micluster/ssh_data.conf<hr>Host <dns name or ip of target server>
      User <username>
      Port <port>
      StrictHostKeyChecking no
      IdentityFile /etc/micluster/id_username@host
  ```
  add an entry for all the targets of the cluster and add the id files with appropriate permissions, e.g. 400

## Usage
- manual
  - start a cluster, program stays in foreground:
    ```
    micluster <cluster>
    ```
  - stop a cluster
    - either stop with KeyboardInterrupt of the foreground program
    - or execute
      ```
      miclusterctl stop <cluster>
      ```
- with systemd, add service file
  <pre>
  /etc/systemd/system/micluster@.service<hr>[Unit]
  Description=micluster for cluster '%i'

  [Service]
  ExecStart=/usr/bin/micluster %i

  [Install]
  WantedBy=multi-user.target
  </pre>
  and execute
  ```
  systemctl daemon-reload
  ```
  and start and stop the service respectively
